<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Spring MVC</title>
</head>
<body>
	<h1>Spring MVC</h1>
	<jsp:include page="menu.jsp"></jsp:include>
	<hr>
	<jsp:include page="form.jsp"></jsp:include>
	<hr>
	<jsp:include page="salut.jsp"></jsp:include>
</body>
</html>