<p>
	<em>Mais le form est dans le fichier form.jsp</em>
</p>
<form action="getForm" method="GET">
	<fieldset>
		<legend>Form 1</legend>
		<label for="prenom">Entrez votre pr�nom</label>
		<input type="text" name="prenom">
		<input type="submit" name="submit" value="Soumettre">
	</fieldset>
</form>
<p>
	<em>Un autre formulaire avec une autre m�thode.</em>
</p>
<form action="getForm2" method="GET">
	<fieldset>
		<legend>Form 2</legend>
		<label for="nom">Entrez votre nom</label>
		<input type="text" name="nom">
		<input type="submit" name="submit" value="Soumettre">
	</fieldset>
</form>