<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!-- @link https://docs.spring.io/spring/docs/4.2.x/spring-framework-reference/html/view.html#view-jsp-formtaglib -->
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Customer</title>
	<style>
		form {
			min-width: 300px;
		}

		form .error {
			color: red;
			font-size:90%;
		}

		form div {
			display: flex;
			margin: 5px auto;
		}

		form label {
			min-width: 100px;
		}

		form input[type="submit"] {
			display: block;
			margin-left: 180px;
		}
	</style>
</head>
<body>
	<h1>Customer</h1>
	<jsp:include page="menu.jsp"></jsp:include>
	<hr>
	<form:form action="customer-result" method="POST" modelAttribute="formCustomer">
		<div>
			<label for="prenom">Pr�nom</label>
			<form:input path="prenom"/>
		</div>
		<div>
			<label for="nom">Nom</label>
			<form:input path="nom"/>
			<form:errors path="nom" cssClass="error"></form:errors>
		</div>
		<div>
			<label for="age">Age</label>
			<form:input path="age" type="number"/>
			<form:errors path="age" cssClass="error"></form:errors>
		</div>
		<div>
			<label for="codePostal">Code Postal</label>
			<form:input path="codePostal"/>
			<form:errors path="codePostal" cssClass="error"></form:errors>
		</div>
		<div>
			<label for="codePromo">Code Promo</label>
			<form:input path="codePromo"/>
			<form:errors path="codePromo" cssClass="error"></form:errors>
		</div>
		<input type="submit" name="submit" value="soumettre">
	</form:form>
	<hr>
	<jsp:include page="customer-result.jsp"></jsp:include>
</body>
</html>