<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Student</title>
</head>
<body>
	<h1>Student</h1>
	<jsp:include page="menu.jsp"></jsp:include>
	<hr>
	<form action="student-result" method="POST" modelAttribute="formStudent">
		<label for="firstname">Pr�nom</label>
		<input type="text" id="firstname" name="firstname">
		<label for="lastname">Nom</label>
		<input type="text" id="lastname" name="lastname">
		<select name="country">
			<option value="false">S�lectionner votre pays</option>
			<option value="fr">France</option>
			<option value="it">Italie</option>
			<option value="en">Royaume-Uni</option>
		</select>
		<input type="submit" name="submit" value="soumettre">
	</form>
	<hr>
	<jsp:include page="student-result.jsp"></jsp:include>
</body>
</html>