<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!-- @link https://docs.spring.io/spring/docs/4.2.x/spring-framework-reference/html/view.html#view-jsp-formtaglib -->
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Student with spring tag</title>
</head>
<body>
	<h1>Student with spring tag</h1>
	<jsp:include page="menu.jsp"></jsp:include>
	<hr>
	<form:form action="student-result-with-spring-tag" method="POST" modelAttribute="formStudent">
		<label for="firstname">Pr�nom</label>
		<form:input path="firstname"/>
		<label for="lastname">Nom</label>
		<form:input path="lastname"/>
		<form:select path="country">
			<form:option value="false" label="S�lectionner votre pays"/>
			<form:options items="${formStudent.countryList}"/>
		</form:select>
		<form:radiobuttons path="gender" items="${formStudent.genderList}"/>
		<form:checkboxes items="${formStudent.osList}" path="os"/>
		<input type="submit" name="submit" value="soumettre">
	</form:form>
	<hr>
	<jsp:include page="student-result-with-spring-tag.jsp"></jsp:include>
</body>
</html>