# Projet Java Spring MVC


## Etapes de développement

1 - Créer la class Controller. Exemple : `HomeController` avec en annotation `@Controller`

2 - Définir la méthode de contrôle. Exemple : `public String ShowMyPage`

3 - Ajouter une annotation qui permet de gérer la requête sur cette méthode ci-dessus : `@RequestMapping("/")`

4 - Retourner une vue. Exemple : `return "fichier1"`

```java

@RequestMapping("/")
public String ShowMyPage() {
	return "fichier1"
}

```

5 - Créer la vue dans le fichier (html de base) qui est dans view/fichier1


## Gestion de datas

1 - Rajouter une méthode au `controller` pour lire l'information et la rajouter au `model`.

2 - Ajouter au .jsp `action="nomDeMethode"` dans le `form`.

3 - Afficher le modèle dans la vue (mettre à jour la vue) avec `${param.nomduparametre}`
