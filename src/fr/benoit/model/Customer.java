package fr.benoit.model;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
//import javax.validation.constraints.Size;

import fr.benoit.validation.CodePromo;

public class Customer {

	@NotNull(message="Le nom doit �tre rempli.") // Utile avec initBinder dans le CustomerController.
//	@Size(min=1, message="is required") // Utile si pas d'initBinder dans le CustomerController.
	private String nom;

	private String prenom;

	@Min(value=18, message="Vous ne pouvez pas commander. Vous �tes mineur.")
	@Max(value=130, message="Euh, normalement vous �tes mort.")
	private int age;

	@NotNull(message="Indiquez votre code postal")
	@Pattern(regexp="^[0-9]{5}", message="5 chiffres demand�s")
	private String codePostal;

	@CodePromo
	private String codePromo;

	public Customer() {
		super();
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	public String getCodePromo() {
		return codePromo;
	}

	public void setCodePromo(String codePromo) {
		this.codePromo = codePromo;
	}

}
