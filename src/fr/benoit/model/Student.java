package fr.benoit.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Student {

	private String firstname;
	private String lastname;
	private String country;
	private String gender;
	private String os;
	private Map<String, String> countryList = new HashMap<>();
	private List<String> genderList = new ArrayList<>();
	private List<String> osList = new ArrayList<>();

	public Student() {
		super();
		this.countryList = createCountryList();
		this.genderList = createGenderList();
		this.osList = createOsList();
	}

	public Map<String, String> createCountryList() {
		countryList.put("sp", "Espagne");
		countryList.put("fr", "France");
		countryList.put("en", "Royaume-Uni");
		return countryList;
	}

	public List<String> createGenderList() {
		genderList.add("femme");
		genderList.add("homme");
		genderList.add("autre");
		return genderList;
	}

	public List<String> createOsList() {
		osList.add("Linux");
		osList.add("Mac OS");
		osList.add("Windows");
		return osList;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Map<String, String> getCountryList() {
		return countryList;
	}

	public void setCountryList(Map<String, String> countryList) {
		this.countryList = countryList;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public List<String> getGenderList() {
		return genderList;
	}

	public void setGenderList(List<String> genderList) {
		this.genderList = genderList;
	}

	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public List<String> getOsList() {
		return osList;
	}

	public void setOsList(List<String> osList) {
		this.osList = osList;
	}

}
