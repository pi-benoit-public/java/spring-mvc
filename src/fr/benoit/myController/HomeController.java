package fr.benoit.myController;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HomeController {

	@RequestMapping("/")
	public String ShowMyPage() {
		return "home";
	}

	@RequestMapping("/getForm")
	public String getForm(HttpServletRequest request, Model model) {
		String myParam = request.getParameter("prenom");
		myParam = myParam.toUpperCase();
		model.addAttribute("newParam", myParam);
		return "home";
	}

	@RequestMapping("/getForm2")
	public String getForm2(@RequestParam("nom") String nom, Model model) {
		String nomUpper = nom.toUpperCase();
		model.addAttribute("newNom", nomUpper);
		return "home";
	}

}
