package fr.benoit.myController;

import javax.validation.Valid;

import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import fr.benoit.model.Customer;

@Controller
public class CustomerController {

	/**
	 * initBinder
	 * Supprime les espaces et consid�re une entr�e comme un string.
	 * @param wdb
	 */
	@InitBinder
	public void initBinder(WebDataBinder wdb) {
		StringTrimmerEditor ste = new StringTrimmerEditor(true);
		wdb.registerCustomEditor(String.class, ste);
	}

	@RequestMapping("/customer")
	public String customerForm(Model model) {
		Customer customer = new Customer();
		model.addAttribute("formCustomer", customer);
		return "customer";
	}

	@RequestMapping("/customer-result")
	public String customerSubmit(@Valid @ModelAttribute("formCustomer") Customer client, BindingResult bindingResult) {
		// Juste pour se rappeler. Pas forc�ment n�cessaire ici. Car les erreurs sont g�r�es avec BindingResult et dans le Customer.
		if (bindingResult.hasErrors()) {
			/*
			 * @todo La v�rification (validation) est faite. Maintenant, il faut traiter les donn�es qui sont attendues.
			 * Si elles ne sont pas ce qui est attendu, ne pas les enregistrer. Donc les bloquer.
			 * Par exemple :
			 */
			if (client.getAge() < 0) {
				client.setAge(0);
			}
			System.out.println("Oups, le nom est obligatoire.");
		}
		return "customer";
	}

}
