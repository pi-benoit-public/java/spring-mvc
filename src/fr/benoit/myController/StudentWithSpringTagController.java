package fr.benoit.myController;

import fr.benoit.model.*;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class StudentWithSpringTagController {

	/**
	 * studentForm
	 * 
	 * Cr�er un �tudiant et l'ajoute � spring pour qu'il op�re sa magie.
	 * @param model
	 * @return
	 */
	@RequestMapping("/student-with-spring-tag")
	public String studentForm2(Model model) {
		Student student = new Student();
		model.addAttribute("formStudent", student);
		return "student-with-spring-tag";
	}

	@RequestMapping("/student-result-with-spring-tag")
	public String studentSubmit2(@ModelAttribute("formStudent") Student monEtudiant) {
		System.out.println(monEtudiant.getGender());
		return "student-with-spring-tag";
	}

}
