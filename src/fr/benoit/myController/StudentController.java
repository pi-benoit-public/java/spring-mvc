package fr.benoit.myController;

import fr.benoit.model.*;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class StudentController {

	/**
	 * studentForm
	 * 
	 * Cr�er un �tudiant et l'ajoute � spring pour qu'il op�re sa magie.
	 * @param model
	 * @return
	 */
	@RequestMapping("/student")
	public String studentForm(Model model) {
		Student student = new Student();
		model.addAttribute("formStudent", student);
		return "student";
	}

	@RequestMapping("/student-result")
	public String studentSubmit(@ModelAttribute("formStudent") Student monEtudiant) {
//		System.out.println(monEtudiant.getFirstname() +" "+ monEtudiant.getLastname());
		return "student";
	}

}
