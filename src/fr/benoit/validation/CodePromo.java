package fr.benoit.validation;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;

import org.springframework.messaging.handler.annotation.Payload;

@Retention(RUNTIME)
@Constraint(validatedBy = CodePromoValidator.class)
@Target({ ElementType.FIELD, ElementType.METHOD })
public @interface CodePromo {

	String value() default "FR";

	String message() default "Code pas bon";

	Class<?>[] groups() default { };

	Class<? extends Payload>[] payload() default { };

}
