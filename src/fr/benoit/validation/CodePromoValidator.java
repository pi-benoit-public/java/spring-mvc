package fr.benoit.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class CodePromoValidator implements ConstraintValidator<CodePromo, String> {

	private String codePromoPrefix;

	@Override
	public void initialize(CodePromo constraintAnnotation) {
		codePromoPrefix = constraintAnnotation.value();
	}

	@Override
	public boolean isValid(String theCode, ConstraintValidatorContext cvc) {
		boolean result;

		if (theCode != null)
			result = theCode.startsWith(codePromoPrefix);
		else
			result = true;

		return result;
	}

}
